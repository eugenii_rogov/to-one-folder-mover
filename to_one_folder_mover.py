from os import listdir
from os.path import isfile, join, exists, splitext

from folder_manager import FolderManager

COVER_EXTENSIONS = {".jpg"}
ARCHIVE_EXTENSIONS = {".zip", ".rar"}


class ToOneFolderMover:
    def __init__(self,
                 root: str,
                 subfolders_prefix: str) -> None:
        """Initialize object

        :param root: Path where share each inner folder
        :param subfolders_prefix: Inner folder prefix for parse
        """
        self._root = root
        self._subfolders_prefix = subfolders_prefix
        self._folder_manager = FolderManager(self._root)

    def move(self) -> None:
        """Files from inner folders of root forler to one folder

        :raises FileNotFoundError: If not root rise error
        """
        if not exists(self._root):
            raise FileNotFoundError(f"File {self._root} is not found")
        one_folder_path = self._folder_manager._create_folder("one_folder")
        covers_path = self._folder_manager._create_folder("covers", main_folder=one_folder_path)
        for folder in listdir(self._root):
            if isfile(folder) or not folder.startswith(self._subfolders_prefix):
                continue
            inner_path = join(self._root, folder)
            for file_name in listdir(inner_path):
                z_extension = splitext(file_name)[1].lower()
                if z_extension in COVER_EXTENSIONS:
                    self._folder_manager._copy_file(folder, covers_path, file_name)
                elif z_extension in ARCHIVE_EXTENSIONS:
                    self._folder_manager._copy_file(folder, one_folder_path, file_name)


# D:\Downloads\Books\Manga\Naruto
# Том
root = input("Enter root path: ")
subfolder_prefix = tuple(input("Enter prefixes: ").split())
mover = ToOneFolderMover(root, subfolder_prefix)
mover.move()
