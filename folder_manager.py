from os import makedirs
from os.path import join, exists
import shutil

from typing import Union


class FolderManager:
    def __init__(self,
                 root: str) -> None:
        """Initialize object

        :param root: Main folder path
        """
        self._root = root

    def _create_folder(self,
                       folder_name: str,
                       main_folder: Union[str, None] = None) -> str:
        """Create folder

        :param folder_name: New folder name
        :param folder: Path where create folder
        :return: New folder path
        """
        if not main_folder:
            main_folder = self._root
        result_path = join(main_folder, folder_name)
        if not exists(result_path):
            makedirs(result_path)
        return result_path

    def _copy_file(self,
                   file: str,
                   path: str,
                   file_name: str) -> None:
        """Copy file to folder 

        :param path: Path to folder
        :param file_name: File name
        """
        new_file_name = "_".join([file, file_name])
        if not exists(join(path, file_name)):
            shutil.copy(join(self._root, file, file_name),
                        join(path, new_file_name))
